<?php
/*
Plugin Name: Custom Price Product
Plugin URI: https://gitlab.com/luism23/api-drissly-woocoomerce-order
Description: hook de WordPress que se activa cuando se completa una orden de WooCommerce.
Version: 1.0.1
Author: https://startscoinc.com/
Author URI: https://startscoinc.com/
License: GPL2
*/
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/api-drissly-woocoomerce-order',
	__FILE__,
	'Api drissly woocoomerce order'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');



use Monolog\Logger;
use Monolog\Handler\StreamHandler;


define("DRISSLY_LOG",true);

define("DRISSLY_PATH",plugin_dir_path(__FILE__));
define("DRISSLY_URL",plugin_dir_url(__FILE__));

require_once DRISSLY_PATH . "log.php";

function mi_funcion( $order_id ) {
    // Obtener los detalles de la orden
    $order = wc_get_order( $order_id );
    // Obtener el correo electrónico y el teléfono del cliente que hizo la compra
    $email = $order->get_billing_email();
    $phone = $order->get_billing_phone();

    // Obtener los detalles del primer producto de la orden
    $items = $order->get_items();

    foreach ( $order->get_items() as $item_id => $item ) {
        $product_id = $item->get_product_id();
        $product_price = $item->get_total();
        $product_id = get_post_meta($product_id,"product_id_drissly",true);
    }

    // Configurar el cuerpo de la solicitud para la petición de inicio de sesión
    $login_body = array(
        'email' => 'alex@pmi-americas.com',
        'password' => 'T78mkU550ePT'
    );

    // Configurar los encabezados de la solicitud para la petición de inicio de sesión
    $login_headers = array(
        'Content-Type' => 'application/json'
    );

    // Enviar la solicitud de inicio de sesión
    $login_response = wp_remote_post(
        'https://acceso.drissly.com/api/v1/auth/login',
        array(
            'headers' => $login_headers,
            'body' => json_encode( $login_body )
        )
    );

    // Obtener el token de la respuesta
    $login_response_body = wp_remote_retrieve_body( $login_response );
    $login_response_data = json_decode( $login_response_body, true );
    $token = $login_response_data['token'];

    // Configurar el cuerpo de la solicitud para la petición de transacción
    $transaction_body = array(
        'phone' => $phone,
        'id_product' => $product_id,
        'amount' => $product_price,
        'aditional' => '',
        'reference' => ''
    );
    
    // Configurar los encabezados de la solicitud para la petición de transacción
    $transaction_headers = array(
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $token
    );

    // Enviar la solicitud de transacción
    $transaction_response = wp_remote_post(
        'https://acceso.drissly.com/api/v1/transaction_service',
        array(
            'headers' => $transaction_headers,
            'body' => json_encode( $transaction_body )
        )
    );

    // Obtener la respuesta
    $transaction_response_code = wp_remote_retrieve_response_code( $transaction_response );
    $transaction_response_body = wp_remote_retrieve_body( $transaction_response );
    // Puedes procesar la respuesta según tus necesidades

    

        // Obtener el mensaje del ticket de la respuesta
        $ticket_response_data = json_decode( $transaction_response_body, true );
        $ticket_message = $ticket_response_data['ticket'];
    
        // Agregar el mensaje del ticket en el correo electrónico de pago completado
        add_filter( 'woocommerce_email_order_meta_fields', function( $fields, $sent_to_admin, $order ) use ( $ticket_message ) {
            if ( ! $sent_to_admin ) {
                $fields['ticket_message'] = array(
                    'label' => __( 'Ticket', 'woocommerce' ),
                    'value' => $ticket_message,
                );
            }
            return $fields;
        }, 10, 3 );
    
}
add_action( 'woocommerce_order_status_completed', 'mi_funcion' );

